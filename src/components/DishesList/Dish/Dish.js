import React from 'react';
import {Button, Card, CardBody, CardImg, CardText, CardTitle, Col, Row} from "reactstrap";

const Dish = (props) => {
    const dish = {
        name:props.name,
        id:props.id,
        price:props.price,
        count:1,
        totalPrice:props.price
    };
    return (
        <Card style={{marginBottom:"10px"}}>
            <Row>
                <Col>
                    <CardImg style={{margin:"20px"}} top width="100%" src={props.image} alt={props.name}/>
                </Col>
                <CardBody>
                    <Row>
                        <Col>
                            <CardTitle>{props.name}</CardTitle>
                            <CardText>{props.price} KGS</CardText>
                        </Col>
                        <Col>
                            <Button onClick={()=>props.addDishOnBasket(dish)} style={{marginTop:"10px"}}>Add to basket</Button>
                        </Col>
                    </Row>
                </CardBody>
            </Row>
        </Card>
    );
};

export default Dish;