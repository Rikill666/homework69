import React from 'react';
import Dish from "./Dish/Dish";

const DishesList = (props) => {
    return (
            <div>
                {Object.keys(props.dishes).map(id => (
                    <Dish addDishOnBasket={props.addDishOnBasket} key={id} image={props.dishes[id].image} price={props.dishes[id].price} name={props.dishes[id].name} id={id}/>
                ))}
            </div>
    );
};

export default DishesList;