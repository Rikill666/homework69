import React from 'react';

const BasketDish = (props) => {
    return (
        <li style={{"listStyleType": "none", cursor: "pointer", marginBottom:"10px"}} onClick={() => props.deleteDishOnBasket(props.id)}>
            {props.name} x{props.count} <strong>{props.totalPrice} KGS</strong>
        </li>
    );
};

export default BasketDish;