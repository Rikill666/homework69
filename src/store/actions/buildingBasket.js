import {DELETE_DISH_ON_BASKET} from "./actionsTypes";

export const deleteDishOnBasket = (dishId) => {
    return {type: DELETE_DISH_ON_BASKET, dishId};
};