import {
    ADD_DISH_ON_BASKET,
    FETCH_DISHES_ERROR,
    FETCH_DISHES_REQUEST,
    FETCH_DISHES_SUCCESS
} from "./actionsTypes";
import axiosOrder from "../../axiosOrders";

export const dishesRequest = () => {
    return {type: FETCH_DISHES_REQUEST};
};

export const dishesSuccess = (dishes) => {
    return {type: FETCH_DISHES_SUCCESS, dishes};
};

export const dishesError = (error) => {
    return {type: FETCH_DISHES_ERROR, error};
};

export const addDishOnBasket = (dishBasket) => {
    return {type: ADD_DISH_ON_BASKET, dishBasket};
};

export const fetchDishes = () => {
    return async dispatch => {
        try{
            dispatch(dishesRequest());
            const response = await axiosOrder.get('/dishes.json');
            dispatch(dishesSuccess(response.data));
        }
        catch (e) {
            dispatch(dishesError(e));
        }
    };
};