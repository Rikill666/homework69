import axiosOrder from "../../axiosOrders";
import {ORDER_ERROR, ORDER_REQUEST, ORDER_SUCCESS} from "./actionsTypes";

export const orderRequest = () => {
    return {type: ORDER_REQUEST};
};

export const orderSuccess = (dishes) => {
    return {type: ORDER_SUCCESS, dishes};
};

export const orderError = (error) => {
    return {type: ORDER_ERROR, error};
};

export const postOrder = (order) => {
    return async dispatch => {
        try{
            dispatch(orderRequest());
            await axiosOrder.post('/orders.json', order);
            dispatch(orderSuccess());
        }
        catch (e) {
            dispatch(orderError(e));
        }
    };
};