import {
    ADD_DISH_ON_BASKET,
    DELETE_DISH_ON_BASKET,
    ORDER_ERROR,
    ORDER_REQUEST,
    ORDER_SUCCESS
} from "../actions/actionsTypes";

import {DELIVERY} from "../../containers/BasketBlock/BasketBlock";

const initialState = {
    dishes: [],
    totalPrice: DELIVERY,
    loading: false,
    error: null,
};
const dishesReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_DISH_ON_BASKET:
            let dishesAdd = [...state.dishes];
            const dishAddIndex = dishesAdd.findIndex(d => action.dishBasket.id === d.id);
            if (dishAddIndex !== -1) {
                let dishAdd = {...dishesAdd[dishAddIndex]};
                dishAdd.count++;
                dishAdd.totalPrice = dishAdd.price * dishAdd.count;
                dishesAdd[dishAddIndex] = dishAdd;
            } else {
                dishesAdd.push(action.dishBasket);
            }
            return {
                ...state,
                dishes: dishesAdd,
                totalPrice: state.totalPrice + action.dishBasket.price
            };
        case DELETE_DISH_ON_BASKET:
            let dishes = [...state.dishes];
            const dishIndex = dishes.findIndex(d => action.dishId === d.id);
            const totalPrice = state.totalPrice - dishes[dishIndex].price * dishes[dishIndex].count
            dishes.splice(dishIndex, 1);
            return {
                ...state,
                dishes: dishes,
                totalPrice: totalPrice
            };
        case ORDER_REQUEST:
            return {...state, loading: true};
        case ORDER_SUCCESS:
            return {...state, error: null,dishes:[], loading: false};
        case ORDER_ERROR:
            return {...state, error: action.error, loading: true};
        default:
            return state;
    }
};


export default dishesReducer;