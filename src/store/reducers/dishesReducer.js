import {FETCH_DISHES_ERROR, FETCH_DISHES_REQUEST, FETCH_DISHES_SUCCESS} from "../actions/actionsTypes";

const initialState = {
    dishes: {},
    error: null,
    loading: false,
};

const dishesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_DISHES_REQUEST:
            return {...state, loading: true};
        case FETCH_DISHES_SUCCESS:
            return {...state, dishes: action.dishes,error:null, loading: false};
        case FETCH_DISHES_ERROR:
            return {...state,error:action.error, loading: true};
        default:
            return state;
    }
};



export default dishesReducer;