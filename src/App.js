import React from 'react';
import {Switch, Route} from "react-router-dom";
import {Container} from "reactstrap";
import DishesBuilder from "./containers/DishesBuilder/DishesBuilder";

const App = () => {
  return (
      <Container>
        <Switch>
          <Route path="/" exact component={DishesBuilder}/>
          <Route render={() => <h1>Not Found</h1>}/>
        </Switch>
      </Container>
  );
};

export default App;