import React, {Component} from 'react';
import {connect} from "react-redux";
import {addDishOnBasket, fetchDishes} from "../../store/actions/buildingDishes";
import DishesList from "../../components/DishesList/DishesList";
import Spinner from "../../components/UI/Spinner/Spinner";

class DishesBlock extends Component {

    componentDidMount() {
        this.props.loadingDishes();
    }

    render() {
        return (
            this.props.dishes ?  this.props.loading ? <Spinner/>:
                <DishesList dishes={this.props.dishes} loading={this.props.loading} addDishOnBasket={this.props.addDishOnBasket}>
                    <Spinner/>
                </DishesList>
                : <h1>No Dishes</h1>
        );
    }
}

const mapStateToProps = state => {
    return {
        dishes: state.dish.dishes,
        loading: state.dish.loading,
        error:state.dish.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        loadingDishes: () => dispatch(fetchDishes()),
        addDishOnBasket: (dishBasket) => dispatch(addDishOnBasket(dishBasket))
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(DishesBlock);