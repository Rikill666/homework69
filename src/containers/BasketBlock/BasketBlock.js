import React, {Component} from 'react';
import {connect} from "react-redux";
import {deleteDishOnBasket} from "../../store/actions/buildingBasket";
import BasketDish from "../../components/BasketDish/BasketDish";
import {Button, Form, FormGroup, Input, Label} from "reactstrap";
import Modal from "../../components/UI/Modal/Modal";
import {postOrder} from "../../store/actions/orderActions";
import Spinner from "../../components/UI/Spinner/Spinner";

export const DELIVERY = 150;
class BasketBlock extends Component {
    state = {
        name: "",
        address: "",
        phone: "",
        show: false
    };

    valueChanged = event => this.setState({[event.target.name]: event.target.value});

    orderHandler = async (event) => {
        event.preventDefault();
        const dishes = this.props.dishes.map(function (dish) {
            return {
                name: dish.name,
                count: dish.count
            }
        });
        const order = {
            dishes: dishes,
            customer: {
                name: this.state.name,
                address: this.state.address,
                phone: this.state.phone
            }
        };
        await this.props.postOrder(order);

    };

    checkoutShow = () => {
        this.setState({show:true});
    };
    checkoutHide = () =>{
        this.setState({show:false});
    };

    render() {
        return (
            this.props.dishes.length > 0 ?
                <div>
                    <ul style={{paddingLeft: 0, paddingTop: "20px"}}>
                        {this.props.dishes.map(dish => {
                            return <BasketDish key={dish.id} totalPrice={dish.totalPrice} count={dish.count}
                                               id={dish.id}
                                               name={dish.name}
                                               deleteDishOnBasket={this.props.deleteDishOnBasket}/>
                        })}
                    </ul>
                    ___________________________________
                    <h6>Delivery: {DELIVERY}</h6>
                    <h6>Total: {this.props.totalPrice}</h6>
                    <Button onClick={this.checkoutShow}>Place order</Button>
                    <Modal show={this.state.show} close={this.checkoutHide}>
                        {this.props.loading ? <Spinner/> :
                            <Form>
                                <FormGroup>
                                    <Label for="exampleName">Name</Label>
                                    <Input
                                        type="text"
                                        name="name"
                                        id="exampleName"
                                        placeholder="Enter your name"
                                        value={this.state.name}
                                        onChange={this.valueChanged}
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="exampleAddress">Address</Label>
                                    <Input
                                        type="text"
                                        name="address"
                                        id="exampleAddress"
                                        placeholder="Enter your address"
                                        value={this.state.address}
                                        onChange={this.valueChanged}
                                    />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="examplePhone">Phone</Label>
                                    <Input
                                        type="tel"
                                        name="phone"
                                        id="examplePhone"
                                        placeholder="Enter your phone"
                                        value={this.state.phone}
                                        onChange={this.valueChanged}
                                    />
                                </FormGroup>
                                <Button onClick={this.orderHandler}>Create order</Button>
                                <Button style={{marginLeft: "10px"}} onClick={this.checkoutHide}>Chanel</Button>
                            </Form>}
                    </Modal>
                </div> : <h1>Choose dishes</h1>
        );
    }
}

const mapStateToProps = state => {
    return {
        dishes: state.basket.dishes,
        totalPrice: state.basket.totalPrice,
        loading: state.basket.loading,
        error: state.basket.error
    };
};

const mapDispatchToProps = dispatch => {
    return {
        deleteDishOnBasket: (dishId) => dispatch(deleteDishOnBasket(dishId)),
        postOrder: (order) => dispatch(postOrder(order))
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(BasketBlock);