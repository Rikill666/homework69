import React, {Component} from 'react';
import {Col, Container, Row} from "reactstrap";
import DishesBlock from "../DishesBlock/DishesBlock";
import BasketBlock from "../BasketBlock/BasketBlock";

class DishesBuilder extends Component {
    render() {
        return (
            <Container style={{marginTop:"15px"}}>
                <Row>
                    <Col sm={6} md={8} lg={8}>
                        <DishesBlock/>
                    </Col>
                    <Col sm={6} md={4} ld={4}>
                        <BasketBlock/>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default DishesBuilder;